// Теоретичне питання
// Поясніть своїми словами, що таке AJAX і чим він корисний при розробці Javascript.
//AJAX - це технологія для асихронної роботи з даними, отриманими від сервера. AJAX корисний нам тим, що дозволяє працювати з кодом непослідовно, тобто не по порядку. Це робить створення коду набагато зручнішим і дозволяє обходити частину помилок.

// Завдання
// Отримати список фільмів серії Зоряні війни та вивести на екран список персонажів для кожного з них.

// Технічні вимоги:
// Надіслати AJAX запит на адресу https://ajax.test-danit.com/api/swapi/films та отримати список усіх фільмів серії Зоряні війни
// Для кожного фільму отримати з сервера список персонажів, які були показані у цьому фільмі. Список персонажів можна отримати з властивості characters.
// Як тільки з сервера буде отримана інформація про фільми, відразу вивести список усіх фільмів на екрані. Необхідно вказати номер епізоду, назву фільму, а також короткий зміст (поля episodeId, name, openingCrawl).
// Як тільки з сервера буде отримано інформацію про персонажів будь-якого фільму, вивести цю інформацію на екран під назвою фільму.

const URL = "https://ajax.test-danit.com/api/swapi/films";
let root = document.querySelector("#root");

class Films {
  request(url) {
    return fetch(url)
      .then((response) => {
        return response.json(); // Парсинг JSON в об'єкт JavaScript
      })
      .catch((error) => {
        console.error("There was a problem with the fetch operation:", error);
      });
  }

  getFilms(url) {
    return this.request(url);
  }

  getCharacters(characters) {
    const charactersList = characters.map((character) => {
      return this.request(character);
    });

    return Promise.allSettled([...charactersList]);
  }

  renderFilms(films) {
    let filmsList = document.createElement("ul");
    let filmItems = films.map(
      (
        {
          episodeId: filmNumber,
          name: filmName,
          openingCrawl: aboutFilm,
          characters,
        },
        index
      ) => {
        let filmItem = document.createElement("li");
        filmItem.innerText = ` ${filmNumber}: ${filmName}; \n About film: ${aboutFilm}`;

        setTimeout(() => {
          this.getCharacters(characters).then((characters) => {
            let planetsList = document.createElement("ul");
            let planetItems = characters.map(({ value }) => {
              let li = document.createElement("li");
              li.textContent = "character: " + value.name;

              return li;
            });
            planetsList.append(...planetItems);
            filmItem.append(planetsList);
          });
        }, 1000 * index);

        return filmItem;
      }
    );
    filmsList.append(...filmItems);
    return filmsList;
  }
}

let film = new Films();

film
  .getFilms(URL)
  .then((films) => {
    console.log(films);

    root.append(film.renderFilms(films));
  })
  .catch((e) => {
    console.log(e);
  });
